<?php
echo "default tag<br>"; //echo is a built -in function to output a string
?>
<?
echo "default tag"; # this is another type of single line comment
?>

<?php
echo "<br>Default Syntax";  # this is Default Syntax
?>

<?
echo "<br>PHP example with short-tags<br>"; # this is Short open Tags
?>

<script language="php">
echo "This is HTML script tags.";   # this is HTML Script Tags
</script>

<%
echo '<br>This is ASP like style';  # this is ASP Style Tags
%>

<?php
echo '<br>Default Syntax';  # this is Default Syntax
?>

<?php
$cars=array("Volvo","Toyota","BMW");
echo "I like" . $cars[o] . " , " .$cars[1] . "and" . $cars[2]. ".";
?>